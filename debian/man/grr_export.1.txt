NAME
  grr_export - export resources from GRR to local filesystem

SYNOPSIS
  grr_export [-h] [--verbose] [--debug] [--config CONFIG]
             [--secondary_configs SECONDARY_CONFIGS] [--config_help]
             [--context CONTEXT] [--plugins PLUGINS]
             [--disallow_missing_config_definitions] [-p PARAMETER]
             [--list_storage] [--username USERNAME] [--reason REASON]
             {collection,collection_files,file,hash_file_store} ...

DESCRIPTION
  This tool can be used to export different kinds of data stored inside the
  GRR instance to the local filesystem. The subcommands decide what kind of
  resource to export.

OPTIONS
  --verbose             Turn of verbose logging.

  --debug               When an unhandled exception occurs break in the
                        debugger.

  --config CONFIG       Primary Configuration file to use. This is normally
                        taken from the installed package and should rarely be
                        specified.

  --secondary_configs SECONDARY_CONFIGS
                        Secondary configuration files to load (These override
                        previous configuration files.).

  --config_help         Print help about the configuration.

  --context CONTEXT     Use these contexts for the config.

  --plugins PLUGINS     Load these files as additional plugins.

  --disallow_missing_config_definitions
                        If true, we raise an error on undefined config
                        options.

  -p PARAMETER, --parameter PARAMETER
                        Global override of config values. For example -p
                        DataStore.implementation=MySQLDataStore

  --list_storage        List all storage subsystems present.

  --username USERNAME   Username to use for export operations authorization.

  --reason REASON       Reason to use for export operations authorization.

SUBCOMMANDS
    collection          Exports RDFValueCollection from AFF4.
    collection_files    Downloads files referenced from the
                        RDFValueCollection.
    file                Downloads files/directories from AFF4.
    hash_file_store     Exports HashFileStore contents.

SEE ALSO
  grr_console(1), grr_fuse(1).

AUTHOR
  GRR developers team <grr-dev@googlegroups.com>
