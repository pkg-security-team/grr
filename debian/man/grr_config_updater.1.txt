NAME
  grr_config_updater - GRR configuration tool

SYNOPSIS
  grr_config_updater [-h] [--verbose] [--debug] [--config CONFIG]
                     [--secondary_configs SECONDARY_CONFIGS] [--config_help]
                     [--context CONTEXT] [--plugins PLUGINS]
                     [--disallow_missing_config_definitions] [-p PARAMETER]
                     [--list_storage][--share_dir SHARE_DIR]
                     <subcommand>

DESCRIPTION
  This is the main configuration tool for GRR. It has numerous subcommands to
  perform various actions. When first setting up a new GRR instance, one
  probably only cares about 'initialize'.

  This tool needs to be executed as the '_grr' user, e.g. via 'sudo -u _grr'.

SUBCOMMANDS
    generate_keys       Generate crypto keys in the configuration.
    repack_clients      Repack the clients binaries with the current
                        configuration.
    initialize          Run all the required steps to setup a new GRR install.
    set_var             Set a config variable.
    update_user         Update user settings.
    add_user            Add a new user.
    delete_user         Delete an user account.
    show_user           Display user settings or list all users.
    upload_raw          Upload a raw file to an aff4 path.
    upload_artifact     Upload a raw json artifact file.
    upload_python       Sign and upload a 'python hack' which can be used to
                        execute code on a client.
    upload_exe          Sign and upload an executable which can be used to
                        execute code on a client.
    sign_component      Authenticode Sign the component.
    upload_component    Sign and upload a client component.
    upload_components   Sign and upload all client components.
    download_missing_rekall_profiles
                        Downloads all Rekall profiles from the repository that
                        are not currently present in the database.

OPTIONS
  --verbose             Turn of verbose logging.

  --debug               When an unhandled exception occurs break in the
                        debugger.

  --config CONFIG       Primary Configuration file to use. This is normally
                        taken from the installed package and should rarely be
                        specified.

  --secondary_configs SECONDARY_CONFIGS
                        Secondary configuration files to load (These override
                        previous configuration files.).

  --config_help         Print help about the configuration.

  --context CONTEXT     Use these contexts for the config.

  --plugins PLUGINS     Load these files as additional plugins.

  --disallow_missing_config_definitions
                        If true, we raise an error on undefined config
                        options.

  -p PARAMETER, --parameter PARAMETER
                        Global override of config values. For example -p
                        DataStore.implementation=MySQLDataStore

  --list_storage        List all storage subsystems present.

  --share_dir SHARE_DIR
                        Path to the directory containing grr data.

AUTHOR
  GRR developers team <grr-dev@googlegroups.com>
