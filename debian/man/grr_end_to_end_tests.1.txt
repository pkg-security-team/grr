NAME
  grr_admin_ui - run tests on GRR clients

SYNOPSIS
  grr_end_to_end_tests [-h] [--verbose] [--debug] [--config CONFIG]
                       [--secondary_configs SECONDARY_CONFIGS]
                       [--config_help] [--context CONTEXT]
                       [--plugins PLUGINS]
                       [--disallow_missing_config_definitions]
                       [-p PARAMETER] [--list_storage] [--local_client]
                       [--local_worker] [--client_ids CLIENT_IDS]
                       [--hostnames HOSTNAMES] [--testnames TESTNAMES]

OPTIONS
  --verbose             Turn of verbose logging.

  --debug               When an unhandled exception occurs break in the
                        debugger.

  --config CONFIG       Primary Configuration file to use. This is normally
                        taken from the installed package and should rarely be
                        specified.

  --secondary_configs SECONDARY_CONFIGS
                        Secondary configuration files to load (These override
                        previous configuration files.).

  --config_help         Print help about the configuration.

  --context CONTEXT     Use these contexts for the config.

  --plugins PLUGINS     Load these files as additional plugins.

  --disallow_missing_config_definitions
                        If true, we raise an error on undefined config
                        options.

  -p PARAMETER, --parameter PARAMETER
                        Global override of config values. For example -p
                        DataStore.implementation=MySQLDataStore

  --list_storage        List all storage subsystems present.

  --local_client        The target client(s) are running locally.

  --local_worker        Run tests with a local worker.

  --client_ids CLIENT_IDS
                        List of client ids to test. If unset we use
                        Test.end_to_end_client_ids from the config.

  --hostnames HOSTNAMES
                        List of client hostnames to test. If unset we use
                        Test.end_to_end_client_hostnames from the config.

  --testnames TESTNAMES
                        List of test names to run. If unset we run all
                        relevant tests.

AUTHOR
  GRR developers team <grr-dev@googlegroups.com>